from django.contrib import admin
from .models import CustomUser
from .forms import CustomUserCreationForm,SignUpForm
from django.contrib.auth.admin import UserAdmin

class CustomUserAdmin(UserAdmin):
    model = CustomUser
    add_form = CustomUserCreationForm

    add_fieldsets = UserAdmin.add_fieldsets + (
        (None, {'fields': ('country','organization','email')}),
    )

    fieldsets = (
        *UserAdmin.fieldsets,
        (
            'User role',
            {
                'fields': (
                    'country',
                    'organization'
                )
            }
        )
    )

admin.site.register(CustomUser, CustomUserAdmin)
