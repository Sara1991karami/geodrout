# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django import forms
from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from .models import CustomUser
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import SetPasswordForm

User = get_user_model()
class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": _("Username"),
                "class": "form-control"
            }
        ))
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "placeholder": _("Password"),
                "class": "form-control"
            }
        ))


class SignUpForm(UserCreationForm):

        username = forms.CharField(required=True,
        widget=forms.TextInput(
            attrs={
                "placeholder": _("Username"),
                "class": "form-control"
            }
        ))
        password1 = forms.CharField(min_length=6, required=True,
        widget=forms.PasswordInput(
            attrs={
                "placeholder": _("Password"),
                "class": "form-control"
            }
        ))
        password2 = forms.CharField(min_length=6, required=True,
        widget=forms.PasswordInput(
            attrs={
                "placeholder": _("Password Confirmation"),
                "class": "form-control"
            }
        ))
        email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "placeholder": "Email",
                "class": "form-control"
            }
        ))
 
        class Meta:
           model = User
           fields = ('username', 'email','password1','password2')
    
        def __init__(self, *args, **kwargs):
         super(SignUpForm, self).__init__(*args, **kwargs)
         self.fields['username'].widget.attrs['class']='form-control'
         self.fields['password1'].widget.attrs['class']='form-control'
         self.fields['password2'].widget.attrs['class']='form-control'
        def clean_username(self):
             username = self.cleaned_data['username']
             if User.objects.exclude(pk=self.instance.pk).filter(username=username).exists():
              raise forms.ValidationError(u'Username "%s" is already in use.' % username)
             return username



class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        #fields = "__all__"
        fields = ['username','first_name','last_name','email','organization',\
		  'country','is_staff']
 
class ChangeCustomUserForm(ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    username = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))

   
    class Meta:
        model = CustomUser
        fields = ['username','first_name','last_name','email','organization',\
		  'country','is_staff']
        widgets = {'first_name': forms.HiddenInput(),
                   'last_name': forms.HiddenInput(),
                    'organization': forms.HiddenInput(),
                    'country': forms.HiddenInput(),
                    'is_staff': forms.HiddenInput(),
                   }
        
class SetPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    new_password2 = forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control'}))
    class Meta:
        model = CustomUser
        fields = ['new_password1', 'new_password2']



   

    
