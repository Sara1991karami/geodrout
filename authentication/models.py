from django.db import models
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    country = models.CharField(_('country'),max_length=100, default='', \
								blank=True)
    organization = models.CharField(_('organisation'),max_length=100, \
							default='', blank=True)