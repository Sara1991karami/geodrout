# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path
from .views import login_view, register_user
from . import views
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('login/', login_view, name="login"),
    path('register/', register_user, name="register"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path('', login_view, name="login"),
    path('index.html', views.index),
    path('update/<int:pk>/', views.edit_user, name="edit_user"),
    path("password_change", views.password_change, name="password_change"),
    
]
