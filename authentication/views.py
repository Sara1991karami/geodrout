# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.models import User
from .forms import LoginForm, SignUpForm,ChangeCustomUserForm
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect
from django.template import loader
from .forms import SetPasswordForm
import pdb

User = get_user_model()

def login_view(request):
    form = LoginForm(request.POST or None)

    msg = None

    if request.method == "POST":

        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                
                # messages.success(request,("Login Successful"))
                return redirect("/")
            else:
                msg = 'Invalid credentials'
        else:
            msg = 'Error validating the form'

    return render(request, "authentication/login.html", {"form": form, "msg": msg})


def register_user(request):
    # msg = None
    # success = False

    if request.method == "POST":

    #   if User.objects.filter(username = request.POST['username']).exists():
         
    #      return HttpResponseRedirect(request.path_info)
      
    
       form = SignUpForm(request.POST)
       if form.is_valid():
            user_exist = User.objects.filter(username = request.POST['username']).exists()
            if not user_exist:
             form.save()
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password1")
            email = form.cleaned_data.get("email")
            user = authenticate(username=username, password=password)
            
            login(request, user)
            
            messages.success(request,("Registration Successful"))
            
            return redirect("/")
       else:
            context= {'form': form, 'error':'The username you entered has already been taken. Please try another username.'}
            # return HttpResponseRedirect(request.path_info)
            return render(request, "authentication/register.html",context)
    
            # msg = 'User created - please <a href="/login">login</a>.'
            # success = True

            # return redirect("/login/")

        # else:
        #     msg = 'Form is not valid'
    else:
        
        form = SignUpForm()
    
    return render(request, "authentication/register.html", {"form": form, "messages":messages})


    
@login_required
def index(request):
    context = {}
    html_template = loader.get_template('user/index.html')
    return HttpResponse(html_template.render(context, request))

@login_required
def edit_user(request, pk):
    user = User.objects.get(pk=pk)
    # print(user)
    # pdb.set_trace()
    user_form = ChangeCustomUserForm(instance=user)

    if request.user.is_authenticated and request.user.id == user.id:
        if request.method == "POST":
            user_form = ChangeCustomUserForm(request.POST, request.FILES, \
								instance=user)
            if user_form.is_valid():
                created_user = user_form.save(commit=False)
                created_user.save()
                return HttpResponseRedirect('/authentication')

        return render(request, "user/account_update.html", {
            "noodle": pk,
            "noodle_form": user_form,
            "user": user,
        })
    else:
        raise PermissionDenied
    

@login_required
def password_change(request):
    user = request.user
    if request.method == 'POST':
        form = SetPasswordForm(user, request.POST)
        if form.is_valid():
            form.save()
            return redirect('/{0:s}'.format(request.LANGUAGE_CODE))
        else:
            for error in list(form.errors.values()):
                messages.error(request, error)

    form = SetPasswordForm(user)
    return render(request, 'user/password_reset_confirm.html', {'form': form})