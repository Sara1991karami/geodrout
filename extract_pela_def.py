from extract_all_bands_sentinel2 import extract_all_bands
from extract_NDVI_sentinel2 import extract_NDVI

    # Parametros do utilizador
extract_area_WGS84 = [-7.166989, 40.792406, -7.165677, 40.79184]   #Configuração da Área de Interesse em coodenadas WGS84
time_interval = ("2023-05-20", "2023-05-30")     # intervalo de tempo que se pretende extrair imagens
out_path_folder ="C:\image" # pasta de distino dos ficheiros
Resolution = 10.0  # resoluçao do pixel na imagem, em float
cloudless_percentage = '100' #sting com a percentagem maxima de nuvens

extract_all_bands(extract_area_WGS84, time_interval, Resolution, cloudless_percentage, out_path_folder)

#extract_NDVI(extract_area_WGS84, time_interval, Resolution, cloudless_percentage, out_path_folder)