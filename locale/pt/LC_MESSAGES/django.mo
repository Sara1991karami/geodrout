��    \      �     �      �  z   �  .   T  ^   �  q   �  !   T	  �   v	  �   �	  0   �
  C   �
  f     �   �     S     e          �     �     �     �     �     �       $   "     G     N     ^     m     �     �     �  5   �     �  
   �     �  	   �       &     	   /     9  
   @     K     Q     X  
   \     g     o  ?   }     �     �     �     �     �          -  =   5  
   s     ~     �     �     �     �     �     �     �     �  B        J     Z     a     h     �     �  #   �     �     �     �               )     /  .   ?     n     �  (   �     �     �  
   �     �     �          
       #  $  s   H  6   �  Z   �  u   N  #   �  �   �  �   {  5   $  =   Z  e   �  �   �     �  #   �          6     J     h     }     �     �     �  $   �               (  !   7     Y     _     f  M   n     �     �     �     �     �  ,   �     $     ,     3     D     T     e     j     w     |  M   �     �     �     �            "   ?     b  F   k  
   �     �     �     �     �        
   )     4     L     Z  J   j     �     �     �     �     �  
   �  +          ,     M  '   ]     �     �     �     �  2   �           )  /   B     r     �     �     �  
   �     �     �     �         (   :                  E          R   &              5   N   4   *   I   -      S          	      D      C      9      B       )       G       +   <                F   ,   3   O   0   J          K       !      %   @   [      '                 \       L   W      =       V      
      M   Y   /          Q                 .   H       ;              Z       A                 8   U   6   2   ?   >              1   #       $   X   7           P       T                  "    
    Forgot your password? Enter your email in the form below and we'll send you instructions for creating a new one.
     
    Sincerely,
    TeroMovigo Management
     
    The following user (%(user)s) has asked to register an account at
    %(site_name)s.
     
    To activate this account, please click the following link within the next
    %(expiration_days)s days:
     
    To approve this, please
     
    We have sent you an email with a link to reset your password. Please check
    your email and click the link to continue.
     
    You have asked to register an account at
    %(site_name)s.  If this wasn't you, please ignore this email
    and your address will be removed from our records.
     
    Your account is now approved. You can 
     
  We have sent an email to %(email)s with further instructions.
   
To reset your password, please click the following link, or copy and paste it
into your web browser:
 
You are receiving this email because you (or someone pretending to be you)
requested that your password be reset on the %(domain)s site. If you do not
wish to reset your password, please ignore this message.
 Account Activated Account Activation Resent Account Approval failed. Account Approved Account activation failed. Activation Failure Activation email sent Approval Failure Best regards Cape Verde GNSS Portal Cape Verde GNSS Portal | TeroMovigo  Caster Change password Change profile Confirm password reset Country E-mail English Enter your new password below to reset your password: Field First name Forgot your password? GNSS Data Hello Here you can update your profile page. Last name Log in Logged out Login Logout Map My profile Network Not a member? Once a site administrator activates your account you can login. Operational Organisation Password changed Password reset Password reset complete Password successfully changed! Planned Please check your email to complete the registration process. Portuguese Profile Register Register for an account Registration is closed Resend Activation Email Reset it Reset password Set password Site chosen Sorry, but registration is closed at this moment. Come back later. Station Details Status Submit Successfully logged out Support TeroMovigo Management The user's account is now approved. This is your profile page. Under construction Update Profile Info Update your information User Profile Value You can log in. You may now <a href="%(login_url)s">log in</a> Your account is now activated. Your password has been reset! Your username, in case you've forgotten: admin approval cancel click here country done log in. organisation registration Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-11-14 13:49+0000
Last-Translator: 
Language-Team: 
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2
 
Esqueceu a sua senha? Insira o seu e-mail no formulário abaixo e enviaremos instruções para criar um novo.
     
    Com os melhores cumprimentos,
    TeroMovigo
     
O seguinte utilizador (%(user)s) pediu para registar uma conta em
    %(site_name)s.
     
    Para activar esta conta, por favor clique no seguinte link dentro do seguinte
    %(expiration_days)s dias:
     
Para aprovar isto, por favor.
     
Enviámos-lhe um e-mail com um link para redefinir a sua palavra-passe. Por favor, verifique
    seu e-mail e clique no link para continuar.
     
    Pediu para registar uma conta em
    %(site_name)s.  Se não foi você, por favor ignore este e-mail
    e o seu endereço será retirado dos nossos registos.
     
A sua conta está agora aprovada. É possível 
     
Enviámos um e-mail para %(email)s com mais instruções.
   
Para redefinir a sua palavra-passe, clique no seguinte link ou copie e cole-o
no seu navegador web:
 
Está a receber este e-mail porque você (ou alguém que finge ser você)
solicitou que a sua palavra-passe fosse reposta no site %(domain)s. Se não o fizer.
deseje redefinir a sua palavra-passe, por favor ignore esta mensagem.
 Conta Ativada A Ativação de Conta foi reenviado A aprovação da conta falhou. Nova Conta Aprovada A ativação da conta falhou. Falha na activação E-mail de ativação enviado Falha da Aprovação Com os melhores cumprimentos Portal GNSS de Cabo Verde Cabo Verde GNSS Portal | TeroMovigo  Caster Alterar palavra-passe Alterar perfil Confirme o reset da palavra-passe País E-mail Inglês Introduza a sua nova palavra-passe abaixo para redefinir a sua palavra-passe: Campo Primeiro Nome Esqueceu sua senha? Dados do GNSS Olá Aqui pode atualizar a sua página de perfil. Apelido Entrar Terminou sessão Iniciar sessão Terminar Sessão Mapa O meu perfil Rede Ainda não se registou? Assim que o administrador do sítio activa a sua conta, pode iniciar sessão. Operacional Organização Senha alterada Repor Palavra-passe Reset de palavra-passe completo A palavra-passe mudou com sucesso! Planeado Por favor, consulte o seu e-mail para completar o processo de registo. Português Perfil Registar Registar-se para uma conta As inscrições estão fechadas Reencaê-lo e-mail de ativação Reinicie-o Redefinir palavra-passe Definir senha Sitio escolhido Desculpe, mas a inscrição está fechada neste momento. Volte mais tarde. Detalhes da estação Estado Enviar Logout bem sucedido Suporte TeroMovigo A conta do utilizador está agora aprovada. Esta é a sua página de perfil. Em construção Informação de perfil de atualização Atualize as suas informações Perfil do Utilizador Valor Pode iniciar sessão. Pode agora <a href="%(login_url)s">fazer login</a> A sua conta está agora ativada. A sua senha foi reposta! O seu nome de utilizador, caso tenha esquecido: aprovação do admin cancelar clique aqui país concluído iniciar sessão. organização registro 