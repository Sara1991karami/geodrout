from django.db import models

# Create your models here.

from django.utils.translation import gettext_lazy as _

# Create your models here.
# class Geotiff(models.Model):
#     name = models.CharField(max_length=255, blank=True)
#     download_date= models.DateTimeField(blank=True, null=True)
#     geotiff=models.ImageField(blank=True, null=True, upload_to='')
#     ph=models.FloatField(max_length=30,default=0.0)
#     humidity=models.FloatField(max_length=30,default=0.0)
#     ndvi=models.FloatField(max_length=30,default=0.0)
#     organic=models.FloatField(max_length=30,default=0.0)
#     uploaded_at = models.DateTimeField(_('uploaded_at'),auto_now_add=True)
   

    
    # def __str__(self):
    #     return str(self.name)
    

class KML(models.Model):
    name = models.CharField(max_length=255, blank=True)
    year= models.DateField(blank=True, null=True)
    uploaded_at = models.DateTimeField(_('uploaded_at'),auto_now_add=True)
   

    
    def __str__(self):
        return str(self.name)
