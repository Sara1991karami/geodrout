from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import KML
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    context = {}
    context['title'] = ''
    

    html_template = loader.get_template('map/viseu.html')
    return HttpResponse(html_template.render(context, request))

@login_required
def viseu(request):

     
    # timeline=Geotiff.objects.all().order_by('download_date')
    kmlfiles=KML.objects.all().order_by('year')
    
    # image_url='https://geodrout.s3.amazonaws.com/media/'
    kml_url='https://geodrout.s3.amazonaws.com/media/'
  
    context={'kml_url':kml_url,'kmlfiles':kmlfiles}
    html_template = loader.get_template('map/viseu.html')
    return HttpResponse(html_template.render(context, request))

@login_required
def guarda(request):
    context = {}
    context['title'] = ''
    

    html_template = loader.get_template('map/guarda.html')
    return HttpResponse(html_template.render(context, request))
