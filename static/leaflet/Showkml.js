// Function to display KML data on the map
function Showkml(kmlUrl) {


  var currentKMLLayer;
    function assignColor(value, minValue, maxValue) {
      const range = (maxValue - minValue).toFixed(0);
      const interval = range / 10;
  
    
      if (value <= minValue + interval) {
        return '0000FF';
      } else if (value <= minValue + interval * 2) {
        return '3355FF'; 
      } else if (value <= minValue + interval * 3) {
        return '338DFF';
      } else if (value <= minValue + interval * 4) {
        return '33FFFC';
      } 
      else if (value <= minValue + interval * 5) {
        return 'B8FF33';
      }     else if (value <= minValue + interval * 6) {
        return 'FFE933';
      }else if (value <= minValue + interval * 7) {
        return 'EFB611';
      }else if (value <= minValue + interval * 8) {
        return 'FFA500';
      }
      else if (value <= minValue + interval * 9) {
        return 'FF5B33';
      }else {
        return 'B60B08';
      }
    }
    
    let minValue = Number.POSITIVE_INFINITY;
    let maxValue = Number.NEGATIVE_INFINITY;
    

    fetch(kmlUrl)
      .then(res => res.text())
      .then(kmltext => {
       
        const parser = new DOMParser();
        const kml = parser.parseFromString(kmltext, 'text/xml');
        const placemarks = kml.getElementsByTagName('Placemark');
        const markers = [];
        const lats = [];
        const longs = [];

        for (let i = 0; i < placemarks.length; i++) {
          const placemark = placemarks[i];
          const description = parseFloat(placemark.querySelector('name').textContent.trim());
          
          
          // console.log('Description:', description);
          // debugger;

  
          minValue = Math.min(minValue, description);
          maxValue = Math.max(maxValue, description);
       
          
  
          const coordinates = placemark.querySelector('coordinates').textContent.trim().split(',');
          const lat = parseFloat(coordinates[1]);
          
          const lng = parseFloat(coordinates[0]);
          
          

          const color = assignColor(description, minValue, maxValue);
          const percentage = (description - minValue) / (maxValue - minValue) * 100;
          // const color = '#' + rainbow.colorAt(percentage);

  
          const markerHtml = `<div style="background-color: #${color}; width: 10px; height: 10px; border-radius: 20%;transform: translate(-10%, -10%)"></div>`;

          lats.push(lat);
          longs.push(lng);

          const marker = L.marker([lat, lng], {
            icon: L.divIcon({
                            html: markerHtml,
                            iconSize: [0,0],
                            iconAnchor: [5, 5],
                            popupAnchor: [10, 10],
                            
             })
        });
       
        const popupContent = `<strong>Description:</strong> ${description}`;
        marker.bindPopup(popupContent); 
     
       
        markers.push(marker);

        }

        
   
        if (map.hasLayer(legend)) {
          map.removeControl(legend);
      }
   
  
      legend.onAdd = function(map) {
      var div = L.DomUtil.create("div", "legend");
      div.id = "legend"
      div.innerHTML += "<h4>Radiation Value: nSv/h </h4>";
      // for (var i = 0; i < 5; i++) {
      // var color = rainbow.colorAt(i * 25);
      // var startValue = minValue + i * (maxValue - minValue) / 5;
      // var endValue = minValue + (i + 1) * (maxValue - minValue) / 5;
      // div.innerHTML += `<i style="background: #${color}"></i><span>${startValue.toFixed(0)}-${endValue.toFixed(0)}</span><br>`;
  
      div.innerHTML += `<i style="background: #0000FF"></i><span>${minValue.toFixed(0)}-${(minValue + (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #3355FF"></i><span>${(minValue+ (maxValue - minValue) / 10).toFixed(0)}-${(minValue + 2 * (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #338DFF"></i><span>${(minValue+ 2 * (maxValue - minValue) / 10).toFixed(0)}-${(minValue + 3 * (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #33FFFC"></i><span>${(minValue+ 3 * (maxValue - minValue) / 10).toFixed(0)}-${(minValue + 4 * (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #B8FF33"></i><span>${(minValue+ 4 * (maxValue - minValue) / 10).toFixed(0)}-${(minValue + 5 * (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #FFE933"></i><span>${(minValue+ 5 * (maxValue - minValue) / 10).toFixed(0)}-${(minValue + 6 * (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #EFB611"></i><span>${(minValue+ 6 * (maxValue - minValue) / 10).toFixed(0)}-${(minValue + 7 * (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #FFA500"></i><span>${(minValue+ 7 * (maxValue - minValue) / 10).toFixed(0)}-${(minValue + 8 * (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #FF5B33"></i><span>${(minValue+ 8 * (maxValue - minValue) / 10).toFixed(0)}-${(minValue + 9 * (maxValue - minValue) / 10).toFixed(0)}</span><br>`;
      div.innerHTML += `<i style="background: #B60B08"></i><span>${(minValue+ 9 * (maxValue - minValue) / 10).toFixed(0)}-${maxValue.toFixed(0)}</span><br>`;
      
  
  // }
  return div;
    };
  legend.addTo(map);
 
  currentKMLLayer = L.layerGroup(markers);
  currentKMLLayer.addTo(map);

const markerClusterGroup = L.markerClusterGroup({
      spiderfyOnMaxZoom: false,
      disableClusteringAtZoom: 2,
      animateAddingMarkers: false 
  });
markers.forEach(marker => markerClusterGroup.addLayer(marker));
const clusterBounds = markerClusterGroup.getBounds();


  map.fitBounds(clusterBounds);

       
      });
  }
  